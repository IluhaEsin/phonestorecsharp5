﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MvcApplicationCSharp5.Migrations
{
    public partial class AddCurrencies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                        INSERT INTO dbo.Currencies
                          (
                              Name,
                              Code,
                              Rate
                          )
                          VALUES
                          (
                              N'Рубль', -- Name - nvarchar
                              N'RUB', -- Code - nvarchar
                              67.0 -- Rate - float
                          ),  
                          (
                              N'Тэнге', -- Name - nvarchar
                              N'TNG', -- Code - nvarchar
                              400.0 -- Rate - float
                          )
                    ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
