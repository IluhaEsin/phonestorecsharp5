﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MvcApplicationCSharp5.Migrations
{
    public partial class AddForeignKeyCurrencyId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                Update Phones 
                SET CurrencyId = (SELECT Id FROM Currencies WHERE Code = 'USD')
                ");

            migrationBuilder.CreateIndex(
                name: "IX_Phones_CurrencyId",
                table: "Phones",
                column: "CurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Phones_Currencies_CurrencyId",
                table: "Phones",
                column: "CurrencyId",
                principalTable: "Currencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropForeignKey(
                name: "FK_Phones_Currencies_CurrencyId",
                table: "Phones");

            migrationBuilder.DropIndex(
                name: "IX_Phones_CurrencyId",
                table: "Phones");
        }
    }
}
