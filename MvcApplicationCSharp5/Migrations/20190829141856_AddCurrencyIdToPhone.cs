﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MvcApplicationCSharp5.Migrations
{
    public partial class AddCurrencyIdToPhone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "Phones",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Phones");
        }
    }
}
