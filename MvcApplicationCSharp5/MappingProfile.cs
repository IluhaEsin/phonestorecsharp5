﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MvcApplicationCSharp5.Models;
using MvcApplicationCSharp5.ViewModels;

namespace MvcApplicationCSharp5
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateCompanyMapping();
            CreateOrderMapping();
            CreatePhonesMapping();
        }

        private void CreateCompanyMapping()
        {
            CreateMap<Company, CompanyViewModel>();
        }

        private void CreateOrderMapping()
        {
            CreateMap<OrderPhoneViewModel, Order>();
        }

        private void CreatePhonesMapping()
        {
            CreateMap<CreatePhoneViewModel, Phone>();
            CreateMap<Phone, EditPhoneViewModel>();
            CreateMap<EditPhoneViewModel, Phone>();
        }
    }
}
