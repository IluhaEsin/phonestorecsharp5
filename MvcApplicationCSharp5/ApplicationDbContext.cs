﻿using Microsoft.EntityFrameworkCore;
using MvcApplicationCSharp5.Models;

namespace MvcApplicationCSharp5
{
    public class ApplicationDbContext : DbContext
    {
        // code first
        public DbSet<Phone> Phones { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Order> Orders { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            #region Phone-Company
            builder.Entity<Phone>()
                .HasOne(p => p.Company)
                .WithMany(c => c.Phones)
                .HasForeignKey(p => p.CompanyId);

            builder.Entity<Company>()
                .HasMany(c => c.Phones)
                .WithOne(p => p.Company)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion

            builder.Entity<Phone>()
                .HasOne(p => p.Currency)
                .WithMany(c => c.Phones)
                .HasForeignKey(p => p.CurrencyId);

            builder.Entity<Currency>()
                .HasMany(c => c.Phones)
                .WithOne(p => p.Currency)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Order>()
                .HasOne(o => o.Currency)
                .WithMany(c => c.Orders)
                .HasForeignKey(p => p.CurrencyId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Currency>()
                .HasMany(c => c.Orders)
                .WithOne(p => p.Currency)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Order>()
                .HasOne(o => o.Phone)
                .WithMany(c => c.Orders)
                .HasForeignKey(p => p.PhoneId);

            builder.Entity<Phone>()
                .HasMany(c => c.Orders)
                .WithOne(p => p.Phone)
                .HasPrincipalKey(c => c.Id);
        }
    }
}
