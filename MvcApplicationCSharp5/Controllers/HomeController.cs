﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcApplicationCSharp5.Models;
using MvcApplicationCSharp5.Services;

namespace MvcApplicationCSharp5.Controllers
{
    public class HomeController : Controller
    {
        private readonly TransientObject _transientObject;
        private readonly ScopedObject _scopedObject;
        private readonly SingletonObject _singletonObject;
        private readonly DemonstrationService _demonstrationService;

        public HomeController(
            TransientObject transientObject,
            ScopedObject scopedObject,
            SingletonObject singletonObject,
            DemonstrationService demonstrationService)
        {
            _transientObject = transientObject;
            _scopedObject = scopedObject;
            _singletonObject = singletonObject;
            _demonstrationService = demonstrationService;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Index", "Phones");
        }

        public IActionResult Privacy()
        {
            ViewBag.Transent1 = _transientObject.Id;
            ViewBag.Scoped1 = _scopedObject.Id;
            ViewBag.SingletonObject1 = _singletonObject.Id;

            ViewBag.Transent2 = _demonstrationService.TransientObject.Id;
            ViewBag.Scoped2 = _demonstrationService.ScopedObject.Id;
            ViewBag.SingletonObject2 = _demonstrationService.SingletonObject.Id;

            return View();
        }

        [HttpPost]
        public IActionResult Hello([FromForm] string userName)
        {
            var user = new User() {Name = userName};
            return View(user);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
