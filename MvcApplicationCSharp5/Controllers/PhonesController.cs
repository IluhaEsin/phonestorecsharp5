﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcApplicationCSharp5.Enums;
using MvcApplicationCSharp5.Models;
using MvcApplicationCSharp5.Repositories;
using MvcApplicationCSharp5.ViewModels;

namespace MvcApplicationCSharp5.Controllers
{
    public class PhonesController : Controller
    {
        private readonly IPhoneRepository _phoneRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly ICurrencyRepository _currencyRepository;

        public PhonesController(
            IPhoneRepository phoneRepository,
            ICompanyRepository companyRepository,
            ICurrencyRepository currencyRepository)
        {
            if(phoneRepository == null)
                throw new ArgumentNullException(nameof(phoneRepository));
            if(companyRepository == null)
                throw new ArgumentNullException(nameof(companyRepository));
            if(currencyRepository == null)
                throw new ArgumentNullException(nameof(currencyRepository));

            _phoneRepository = phoneRepository;
            _companyRepository = companyRepository;
            _currencyRepository = currencyRepository;
        }

        // GET: Phones
        public IActionResult Index(
            string name,
            int? companyId,
            decimal? priceFrom,
            decimal? priceTo,
            SortState sortState = SortState.NameAsc,
            int page = 1)
        {
            var phones = _phoneRepository.GetAllIncludeCompanies();
            var model = new PhonesListViewModel();
            #region Filters
            if (!string.IsNullOrWhiteSpace(name))
            {
                phones = phones.Where(p => p.Name.Contains(name));
            }

            if (companyId.HasValue)
            {
                phones = phones.Where(p => p.CompanyId == companyId.Value);
            }

            if (priceFrom.HasValue)
            {
                phones = phones.Where(p => p.Price >= priceFrom.Value);
            }

            if (priceTo.HasValue)
            {
                phones = phones.Where(p => p.Price <= priceTo.Value);
            }
            #endregion

            #region Sort
            model.NameSortState = sortState == SortState.NameAsc ? SortState.NameDesc : SortState.NameAsc;
            model.CompanySortState = sortState == SortState.CompanyAsc ? SortState.CompanyDesc : SortState.CompanyAsc;
            model.PriceSortState = sortState == SortState.PriceAsc ? SortState.PriceDesc : SortState.PriceAsc;

            switch (sortState)
            {
                case SortState.NameDesc:
                    phones = phones.OrderByDescending(s => s.Name);
                    break;
                case SortState.CompanyAsc:
                    phones = phones.OrderBy(s => s.Company.Name);
                    break;
                case SortState.CompanyDesc:
                    phones = phones.OrderByDescending(s => s.Company.Name);
                    break;
                case SortState.PriceAsc:
                    phones = phones.OrderBy(s => s.Price);
                    break;
                case SortState.PriceDesc:
                    phones = phones.OrderByDescending(s => s.Price);
                    break;
                default:
                    phones = phones.OrderBy(s => s.Name);
                    break;

            }
            #endregion

            int pageSize = 3;
            var count = phones.Count();
            phones = phones.Skip((page - 1) * pageSize).Take(pageSize);

            model.Phones = phones.ToList();
            model.Companies = new SelectList(_companyRepository.GetAll(), "Id", "Name");
            model.Name = name;
            model.CompanyId = companyId;
            model.PriceTo = priceTo;
            model.PriceFrom = priceFrom;
            model.CurrentSortState = sortState;
            model.PageViewModel = new PageViewModel(count, page, pageSize);

            return View(model);
        }

        // GET: Phones/Details/5
        public IActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            var phone = _phoneRepository.GetById(id.Value);
            if (phone == null)
            {
                return NotFound();
            }

            return View(phone);
        }

        // GET: Phones/Create
        public IActionResult Create()
        {
            var createPhoneViewModel = new CreatePhoneViewModel
            {
                Companies = new SelectList(_companyRepository.GetAll(), "Id", "Name"),
                Currencies = new SelectList(_currencyRepository.GetAll(), "Id", "Name")
            };
            return View(createPhoneViewModel);
        }

        // POST: Phones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreatePhoneViewModel model)
        {
            if (ModelState.IsValid)
            {
                var phone = Mapper.Map<Phone>(model);
                _phoneRepository.Add(phone);
                return RedirectToAction(nameof(Index));
            }

            model.Companies = new SelectList(_companyRepository.GetAll(), "Id", "Name");
            model.Currencies = new SelectList(_currencyRepository.GetAll(), "Id", "Name");
            return View(model);
        }

        // GET: Phones/Edit/5
        public IActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            var phone = _phoneRepository.GetById(id.Value);
            if (phone == null)
            {
                return NotFound();
            }

            var model = Mapper.Map<EditPhoneViewModel>(phone);
            model.Companies = new SelectList(_companyRepository.GetAll(), "Id", "Name", model.CompanyId);
            model.Currencies = new SelectList(_currencyRepository.GetAll(), "Id", "Name", model.CurrencyId);
            return View(model);
        }

        // POST: Phones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditPhoneViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var phone = Mapper.Map<Phone>(model);

                    _phoneRepository.Update(phone);
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return BadRequest(ex.Message);
                }
                return RedirectToAction(nameof(Index));
            }
            model.Companies = new SelectList(_companyRepository.GetAll(), "Id", "Name", model.CompanyId);
            model.Currencies = new SelectList(_currencyRepository.GetAll(), "Id", "Name", model.CurrencyId);
            return View(model);
        }

        // GET: Phones/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var phone = _phoneRepository.GetById(id.Value);
            if (phone == null)
            {
                return NotFound();
            }

            return View(phone);
        }

        // POST: Phones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var phone = _phoneRepository.GetById(id);
            _phoneRepository.Delete(phone);
            return RedirectToAction(nameof(Index));
        }

        private bool PhoneExists(int id)
        {
            return _phoneRepository.GetById(id) != null;
        }
    }
}
