﻿using System.IO;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols;
using MvcApplicationCSharp5.Models;
using MvcApplicationCSharp5.Repositories;
using MvcApplicationCSharp5.ViewModels;

namespace MvcApplicationCSharp5.Controllers
{
    public class CompaniesController : Controller
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IConfiguration _configuration;

        public CompaniesController(
            ICompanyRepository companyRepository,
            IConfiguration configuration)
        {
            _companyRepository = companyRepository;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            var companies = _companyRepository.GetAll();
            return View(companies);
        }

        public IActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            var company = _companyRepository.GetById(id.Value);
            
            var imagesFilesPath = _configuration["ImagesPath"];

            var viewModel = Mapper.Map<CompanyViewModel>(company);

            viewModel.LogoFilePath = string.IsNullOrWhiteSpace(company.LogoFileName) ?
                                    string.Empty :
                                    Path.Combine(imagesFilesPath, company.LogoFileName);
            
            return View(viewModel);
        }

        public IActionResult Download(int? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            var company = _companyRepository.GetById(id.Value);
            var filePath = Path.Combine("~", "files", company.Name + ".pdf");

            return File(filePath, "application/pdf", "book.pdf");
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Company company)
        {
            _companyRepository.Add(company);

            return RedirectToAction(nameof(Index));
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckCompany(string name)
        {
            var company = _companyRepository.FindByName(name);

            if (company != null)
                return Json($"Компания {name} уже существует");

            return Json(true);
        }
    }
}