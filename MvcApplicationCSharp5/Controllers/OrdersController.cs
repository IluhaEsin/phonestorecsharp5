﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcApplicationCSharp5;
using MvcApplicationCSharp5.Models;
using MvcApplicationCSharp5.Repositories;
using MvcApplicationCSharp5.ViewModels;

namespace MvcApplicationCSharp5.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IOrdersRepository _ordersRepository;
        private readonly IPhoneRepository _phoneRepository;
        private readonly ICurrencyRepository _currencyRepository;

        public OrdersController(
            IOrdersRepository ordersRepository,
            IPhoneRepository phoneRepository,
            ICurrencyRepository currencyRepository)
        {
            _ordersRepository = ordersRepository;
            _phoneRepository = phoneRepository;
            _currencyRepository = currencyRepository;
        }
        //private readonly ApplicationDbContext _context;

        //public OrdersController(ApplicationDbContext context)
        //{
        //    _context = context;
        //}

        // GET: Orders
        //public async Task<IActionResult> Index()
        //{
        //    var applicationDbContext = _context.Orders.Include(o => o.Currency).Include(o => o.Phone);
        //    return View(await applicationDbContext.ToListAsync());
        //}

        // GET: Orders/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var order = await _context.Orders
        //        .Include(o => o.Currency)
        //        .Include(o => o.Phone)
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (order == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(order);
        //}

        // GET: Orders/Create
        public IActionResult Create(int? phoneId)
        {
            if (!phoneId.HasValue)
            {
                return RedirectToAction("Index", "Phones");
            }

            var phone = _phoneRepository.GetById(phoneId.Value);

            if (phone == null)
            {
                return RedirectToAction("Index", "Phones");
            }

            var orderPhoneViewModel = new OrderPhoneViewModel()
            {
                PhoneId = phone.Id,
                PhoneName = phone.Name,
                CurrenciesSelectList = new SelectList(_currencyRepository.GetAll(), "Id", "Name")
            };
            
            return View(orderPhoneViewModel);
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([FromForm] OrderPhoneViewModel model)
        {
            try
            {
                if(IsOrderAlreadyExists(model))
                    ModelState.AddModelError("Error", "такой заказ уже есть");

                if (!ModelState.IsValid)
                    return View("Create", model);

                var order = Mapper.Map<Order>(model);

                _ordersRepository.Add(order);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", e.Message);
                return RedirectToAction("Create", new
                {
                    phoneId = model.PhoneId
                });
            }

            return RedirectToAction("Details", "Phones", new
            {
                id = model.PhoneId
            });
        }

        private bool IsOrderAlreadyExists(OrderPhoneViewModel model)
        {
            var order = _ordersRepository.FindOrderByClientAndPhoneId(model.PhoneId, model.CustomerName,
                model.CustomerAddress);

            return order != null;
        }
    }
}
