﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcApplicationCSharp5.Services
{
    public class DemonstrationService
    {
        public TransientObject TransientObject { get; }
        public ScopedObject ScopedObject { get; }
        public SingletonObject SingletonObject { get; }

        public DemonstrationService(TransientObject transientObject, ScopedObject scopedObject, SingletonObject singletonObject)
        {
            TransientObject = transientObject;
            ScopedObject = scopedObject;
            SingletonObject = singletonObject;
        }


    }
}
