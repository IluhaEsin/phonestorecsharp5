﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcApplicationCSharp5.Services
{
    public class TransientObject
    {
        public Guid Id { get; }

        public TransientObject()
        {
            Id = Guid.NewGuid();
        }
    }

    public class ScopedObject
    {
        public Guid Id { get; }

        public ScopedObject()
        {
            Id = Guid.NewGuid();
        }
    }

    public class SingletonObject
    {
        public Guid Id { get; }

        public SingletonObject()
        {
            Id = Guid.NewGuid();
        }
    }

    
}
