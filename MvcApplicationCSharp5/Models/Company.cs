﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace MvcApplicationCSharp5.Models
{
    public class Company : Entity
    {
        public Company()
        {
            Phones = new List<Phone>();
        }

        [Remote(action: "CheckCompany", controller: "Companies")]
        public string Name { get; set; }

        public string LogoFileName { get; set; }

        public ICollection<Phone> Phones { get; set; }
    }
}