﻿using System.Collections;
using System.Collections.Generic;

namespace MvcApplicationCSharp5.Models
{
    public class Phone : Entity
    {
        public string Name { get; set; }
        public int CompanyId { get; set; }
        public int CurrencyId { get; set; }
        public decimal Price { get; set; }

        public Company Company { get; set; }
        public Currency Currency { get; set; }

        public ICollection<Order> Orders { get; set; }

        public Phone()
        {
            Orders = new List<Order>();
        }
    }
}
