﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcApplicationCSharp5.Models
{
    public class Currency : Entity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public double Rate { get; set; }
        public ICollection<Phone> Phones { get; set; }
        public ICollection<Order> Orders { get; set; }

        public Currency()
        {
            Phones = new List<Phone>();
            Orders = new List<Order>();
        }
    }
}
