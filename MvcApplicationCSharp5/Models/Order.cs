﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcApplicationCSharp5.Models
{
    public class Order : Entity
    {
        public DateTime DateCreated { get; set; }

        public string CustomerName { get; set; }

        public string CustomerAddress { get; set; }

        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }

        public int PhoneId { get; set; }
        public Phone Phone { get; set; }

        public Order()
        {
            DateCreated = DateTime.Now;
        }
    }
}
