﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MvcApplicationCSharp5.ViewModels
{
    public class CreatePhoneViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int CompanyId { get; set; }
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        [Range(1.0D, 1000000.0D)]
        public decimal Price { get; set; }

        public SelectList Companies { get; set; }
        public SelectList Currencies { get; set; }
    }
}
