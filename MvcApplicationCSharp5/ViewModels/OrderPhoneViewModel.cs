﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using MvcApplicationCSharp5.Models;

namespace MvcApplicationCSharp5.ViewModels
{
    public class OrderPhoneViewModel
    {
        public int PhoneId { get; set; }
        public string PhoneName { get; set; }
        public SelectList CurrenciesSelectList { get; set; }

        [Required(ErrorMessage = "Поле имя заказчика должно быть заполненным")]
        [Display(Name = "Имя заказчика")]
        public string CustomerName { get; set; }
        [Display(Name = "Адрес заказчика")]
        public string CustomerAddress { get; set; }

        [Required]
        [Display(Name = "Валюта платежа")]
        public int CurrencyId { get; set; }
    }
}
