﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using MvcApplicationCSharp5.Enums;
using MvcApplicationCSharp5.Models;

namespace MvcApplicationCSharp5.ViewModels
{
    public class PhonesListViewModel
    {
        public List<Phone> Phones { get; set; }

        public SelectList Companies { get; set; }

        public string Name { get; set; }
        public int? CompanyId { get; set; }
        public decimal? PriceFrom { get; set; }
        public decimal? PriceTo { get; set; }

        public SortState NameSortState { get; set; }
        public SortState CompanySortState { get; set; }
        public SortState PriceSortState { get; set; }

        public SortState CurrentSortState { get; set; }

        public PageViewModel PageViewModel { get; set; }
    }
}
