﻿using System.Linq;
using MvcApplicationCSharp5.Models;

namespace MvcApplicationCSharp5.Repositories
{
    public interface ICompanyRepository : IRepository<Company>
    {
        Company FindByName(string name);
    }

    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        public CompanyRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Companies;
        }

        public Company FindByName(string name)
        {
            return DbSet.FirstOrDefault(c => c.Name == name);
        }
    }
}
