﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvcApplicationCSharp5.Models;

namespace MvcApplicationCSharp5.Repositories
{
    public interface ICurrencyRepository : IRepository<Currency>
    {

    }

    public class CurrencyRepository : Repository<Currency>, ICurrencyRepository
    {
        public CurrencyRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Currencies;
        }
    }
}
