﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvcApplicationCSharp5.Models;

namespace MvcApplicationCSharp5.Repositories
{
    public interface IOrdersRepository : IRepository<Order>
    {
        Order FindOrderByClientAndPhoneId(int phoneId, string customerName, string customerAddress);
    }

    public class OrdersRepository : Repository<Order>, IOrdersRepository
    {
        public OrdersRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Orders;
        }

        public Order FindOrderByClientAndPhoneId(int phoneId, string customerName, string customerAddress)
        {
            return DbSet.FirstOrDefault(p =>
                p.PhoneId == phoneId && p.CustomerName == customerName && p.CustomerAddress == customerAddress);
        }
    }
}
