﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcApplicationCSharp5.Enums
{
    public enum SortState
    {
        NameAsc,
        NameDesc,
        CompanyAsc,
        CompanyDesc,
        PriceAsc,
        PriceDesc
    }
}
